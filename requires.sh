#!/bin/bash


apt-get install \
memtest86+ \
coreutils \
dialog \
mkisofs | genisoimage \
findutils \
bash \
passwd \
sed \
squashfs-tools \
casper \
rsync \
mount \
eject \
libdebian-installer4 \
os-prober \
ubiquity-frontend-debconf \
user-setup \
discover1 | discover \
laptop-detect \
syslinux \
util-linux \
xresprobe
